# `powerline.bash`

`powerline.bash` est une invite de commande dynamique dans le style Powerline,
pour BASH.

![capture d'écran](screenshot.png)


## Fonctionalités

- Configurable par des variables d'env. Pas de fichier.
- Sur deux lignes pour faciliter le *copicollage*.
- `utilisateur@hôte` si dans un client SSH ou sudo, *coloré dynamiquement*.
- Dossier courant, compressé avec l'initiale si trop long.
- Nom du *virtualenv* Python.
- Infos *git* : branche courante, modifications non validées, désynchronisation
  avec la branche amont.
- Coloration du `$` selon l'état de la précédente commande.
- *Très rapide*. Entre 5 et 20 fois plus rapide que `powerline-shell`.
- S'installe facilement. *Un seul fichier sans dépendances*.
- *Extensible*.


## Installation

``` console
$ curl -Lo ~/.config/powerline.bash https://gitlab.com/bersace/powerline-bash/raw/master/powerline.bash
$ $EDITOR ~/.bashrc
# Copier et adapter ceci dans votre .bashrc
. ${HOME}/.config/powerline.bash
PROMPT_COMMAND='__update_ps1 $?'
$ exec $SHELL
```

Les chevrons de Powerline requiert une police adaptée. Il y en a plein à
https://nerdfonts.com/ . J'utilise Sauce Code Pro.


## Configuration

Les variables d'env sont prise-en-compte sans recharger le shell.

`POWERLINE_SEGMENTS` liste les infos à afficher. Par défaut `hostname pwd venv
git status`.

`POWERLINE_STYLE` le style d'affichage. Par défaut `default`.


## Références

- https://github.com/b-ryan/powerline-shell
- https://github.com/skeswa/prompt
