#!/bin/bash
#
# Merge of https://github.com/b-ryan/powerline-shell/ and
# https://github.com/skeswa/prompt .
#
# Configuration:
#
# POWERLINE_SEGMENTS="pwd venv git"
# POWERLINE_STYLE="default" or your
#
# These variables can be set per directory.

# This value is used to hold the return value of the prompt sub-functions. This
# hack avoid calling functions un subprocess to get ret from stdout.
__powerline_retval=""

# Gets the current working directory path, but shortens the directories in the
# middle of long paths to just their respective first letters.
function __powerline_shorten_dir {
  # Break down the local variables.
  local dir="$1"
  IFS=/; dir_parts=(${dir##/}); unset IFS
  local number_of_parts=${#dir_parts[@]}

  # If there are less than 6 path parts, then do no shortening.
  if [[ "$number_of_parts" -gt "5" ]]; then
    # Leave the last 2 part parts alone.
    local last_index="$(( $number_of_parts - 3 ))"
    local short_pwd=""

    # Check for a leading slash.
    if [[ "${dir:0:1}" == "/" ]]; then
      # If there is a leading slash, add one to `short_pwd`.
      short_pwd+='/'
    fi

    for i in "${!dir_parts[@]}"; do
      # Append a '/' before we do anything (provided this isn't the first part).
      if [[ "$i" -gt "0" ]]; then
        short_pwd+='/'
      fi

      # Don't shorten the first/last few arguments - leave them as-is.
      if [[ "$i" -lt "2" || "$i" -gt "$last_index" ]]; then
        short_pwd+="${dir_parts[i]}"
      else
        # This means that this path part is in the middle of the path. Our logic
        # dictates that we shorten parts in the middle like this.
        short_pwd+="${dir_parts[i]:0:1}"
      fi
    done

    # Return the resulting short pwd.
    __powerline_retval="$short_pwd"
  else
    # We didn't change anything, so return the original pwd.
    __powerline_retval="$dir"
  fi
}

# Parses git status --porcelain=v2 output in an array
function __powerline_parse_git_status() {
    local status="$1"
    # branch infos as returned by status : sha, name, upstream, ahead/behind
    local branch_infos=()
    local sha
    local dirty=
    local ab
    local detached=

    while read line ; do
        # If line starts with '# ', it's branch info
        if [ -z "${line### branch.*}" ] ; then
            branch_infos+=("${line#\# branch.* }")
        else
            # Else, it's a changes. The worktree is dirty
            dirty=1
            break
        fi
    done <<< "${status}"

    # Try to provide a meaningful info if we are not on a branch.
    if [ "${branch_infos[1]}" == "(detached)" ] ; then
        detached=1
        if desc="$(git describe --tags --abbrev=7 2>/dev/null)" ; then
            branch="${desc}"
        else
            # Au pire des cas, utiliser la SHA du commit courant.
            branch="${branch_infos[0]:0:7}"
        fi
    else
        branch="${branch_infos[1]}"
    fi

    ab="${branch_infos[3]-}"
    __powerline_retval=("${branch}" "${dirty}" "${ab}" "${detached}")
}


# A segment is a bash function starting with `__powerline_segment_` and putting an
# array of segment info in `__powerline_retval`. A segment info is a string with
# the format : `<bg_color>:<fg_color>:<segment text>`.

declare -A __powerline_dark_colors
__powerline_init_dark_colors() {
    # Liste les sequences de couleurs sombres.
    local sequences=(
        "  0   6  7"
        "  2   3 15"
        "  6   6  15"
        "  8   8  7"
        " 16  30  7"
        " 52  58  7"
        " 59  66 15"
        " 88  93  7"
        " 94 102 15"
        "124 133  7"
        "134 136 15"
        "232 240  7"
        "240 245 15"
    )
    local sequence
    local i

    # __powerline_dark_colors est un tableau associatif couleur sombre ->
    # couleur de texte.
    for sequence in "${sequences[@]}" ; do
        sequence=($sequence)
        for i in $(seq ${sequence[0]} ${sequence[1]}) ; do
            # Par défaut, on met la couleur 7, gris très clair, comme couleur de
            # texte.
            __powerline_dark_colors[$i]=${sequence[2]-7}
        done
    done
}
__powerline_init_dark_colors

__powerline_segment_hostname() {
    local bg
    local fg
    local text

    text="${LOGNAME-$(whoami)}@${HOSTNAME-$(hostname --short)}"
    # Calculer la couleur à partir du texte à afficher.
    texth=$(sum <<< "${text}")
    let bg="1${texth// /} % 256"
    # Assurer la lisibilité en déterminant la couleur du texte en fonction de la
    # clareté du fond.
    fg=${__powerline_dark_colors[$bg]-0}

    __powerline_retval=("48;5;${bg}:38;5;${fg}:${text}")
}

__powerline_segment_pwd() {
    local short_pwd

    __powerline_shorten_dir "$(dirs +0)"
    short_pwd="${__powerline_retval}"

    __powerline_retval=()
    # If we are in HOME, put it in another segment.
    if [ -z "${short_pwd##\~*}" ] ; then
        __powerline_retval+=("48;5;31:38;5;15:~")
        short_pwd="${short_pwd##\~}"
        short_pwd="${short_pwd##/}"
    fi
    # Aérer les / avec des espaces
    IFS=/; parts=(${short_pwd}); unset IFS
    printf -v short_pwd " / %s" "${parts[@]}"
    short_pwd="${short_pwd:3}"
    # Génerer le dernier segment
    __powerline_retval+=("48;5;237:38;5;250:${short_pwd}")
}

__powerline_segment_venv() {
    if [ -z "${VIRTUAL_ENV-}" ] ; then
        __powerline_retval=()
        return
    fi

    __powerline_retval=("48;5;35:38;5;0:${VIRTUAL_ENV##*/}")
}

__powerline_segment_status() {
    local ec=$1

    if [ $ec -eq 0 ] ; then
        __powerline_retval=()
        return
    fi

    # Mettre en gras avec ;1
    __powerline_retval=("48;5;234:38;5;1;1:✘ $ec")
}

__powerline_segment_git() {
    local branch
    local branch_colors
    local ab
    local ab_segment=''
    local detached

    if ! status="$(LC_ALL=C.UTF-8 git status --porcelain=v2 --branch 2>/dev/null)" ; then
        __powerline_retval=()
        return
    fi
    __powerline_parse_git_status "${status}"
    branch="${__powerline_retval[0]}"
    ab="${__powerline_retval[2]}"
    detached="${__powerline_retval[3]}"

    # Colorer la branche selon l'existance de modifications.
    if [ -n "${__powerline_retval[1]}" ] ; then
        branch_colors="48;5;161:38;5;15"
    else
        branch_colors="48;5;148:38;5;0"
    fi

    __powerline_retval=("${branch_colors}:${detached:+⚓ }${branch}")

    # Compute ahead/behind segment
    if [ -n "${ab##+0*}" ] ; then
        # No +0, the local branch is ahead upstream.
        ab_segment="⬆"
    fi
    if [ -n "${ab##+* -0}" ] ; then
        # No -0, the local branch is behind upstream.
        ab_segment+="⬇"
    fi
    if [ -n "${ab_segment}" ] ; then
        __powerline_retval+=("48;5;240:38;5;250:${ab_segment}")
    fi
}


# A render function is a bash function starting with `__powerline_render_`. It puts
# a PS1 string in `__powerline_retval`.

__powerline_render_default() {
    local ps=''
    # Préfixe de segment. Le premier segment a un espace.
    local sp=' '
    local segment
    local text
    for segment in "${__powerline_segments[@]}" ; do
        IFS=':'; infos=($segment); unset IFS
        local old_bg=${bg-}
        # Recoller les entrées 2 et suivantes avec :
        printf -v text ":%s" "${infos[@]:2}"
        text=${text:1}
        # Nettoyer le \n ajouté par <<<
        text="${text##[[:space:]]}"
        text="${text%%[[:space:]]}"
        # Sauter les segments vides
        if [ -z "${text}" ] ; then
            continue
        fi

        # D'abord, afficher le chevron avec la transition de fond.
        local bg=${infos[0]%%:}
        local fg=${infos[1]%%:}
        if [ -n "${old_bg}" ] ; then
            ps+="\[\e[${old_bg/48;/38;}m\e[${bg}m\] "
        fi
        # Ensuite, afficher le segment, coloré
        ps+="\[\e[${bg}m\e[${fg}m\]${sp-}${text} "
        # Vider le préfixe de segment
        sp=
    done

    # Afficher le dernier chevron, transition du fond vers rien.
    old_bg=${bg-}
    bg='49'
    if [ -n "${old_bg}" ] ; then
        ps+="\[\e[${old_bg/48;/38;}m\e[${bg}m\] "
    fi

    # Retourner l'invite de commande
    __powerline_retval="${ps}"
}


# Show dollar line.
__powerline_dollar() {
    local last_exit_code=$1
    # Déterminer la couleur du dollar
    if [ $last_exit_code -gt 0 ] ; then
        fg=${ERROR_FG-"1;38;5;161"}
    else
        fg=0
    fi
    # Afficher le dollar sur une nouvelle ligne, pas en mode powerline
    __powerline_retval="\[\e[${fg}m\]\\\$\[\e[0m\] "
}

__update_ps1() {
    local last_exit_code=${1-$?}
    __powerline_segments=()
    # Détecter si on est connecté sur une autre machine ou un autre utilisateur.
    local other=${SSH_CLIENT-${SUDO_USER-}}
    local segments=${POWERLINE_SEGMENTS-${other:+hostname} pwd venv git status}
    local segment
    for segment in ${segments} ; do
        __powerline_segment_${segment} $last_exit_code
        __powerline_segments+=("${__powerline_retval[@]}")
    done

    PS1=""
    __powerline_render_${POWERLINE_STYLE-default}
    PS1+="${__powerline_retval}"
    __powerline_dollar $last_exit_code
    PS1+="\n${__powerline_retval}"
}

if [ -z "${PROMPT_COMMAND-}" ] ; then
    PROMPT_COMMAND='__update_ps1 $?'
fi
