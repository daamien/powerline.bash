#!/bin/bash -eu

. ./powerline.bash

HOSTNAME=$(hostname --short)
for bg in $(seq 0 255) ; do
    fg=${__powerline_dark_colors[$bg]-0}
    echo -e "\e[38;5;${fg};48;5;${bg}m ${LOGNAME}@${HOSTNAME} \e[0m ${fg}/${bg}";
done
